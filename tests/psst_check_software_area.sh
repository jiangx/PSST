#!/bin/bash
#
# Description:
#   script to check cms software directory
#

############################################################
PSST_ISSUE_TEXT="PSST_ISSUE_TEXT: "
exit_code=0

# set default number of cpu
cpus=$1
if [ "x$cpus" = "x" ]; then
  cpus=1
fi

# -------------------------------------------------------- #
# Software area definition and existence
# -------------------------------------------------------- #
hasCVMFS=0
/usr/bin/cvmfs_config stat -v cms.cern.ch > /dev/null 2>&1
if [ $? -eq 0 ]; then
    hasCVMFS=1
fi

if [ -n "$OSG_GRID" ] ; then
  [ -f $OSG_GRID/setup.sh ] && source $OSG_GRID/setup.sh
  if [ -d $OSG_APP/cmssoft/cms ] ;then
    SW_DIR=$OSG_APP/cmssoft/cms
  elif [ -d $CVMFS/cms.cern.ch ] ; then
    SW_DIR=$CVMFS/cms.cern.ch
  elif [ -d /cvmfs/cms.cern.ch ] ; then
    SW_DIR=/cvmfs/cms.cern.ch
  else
    message="${ERROR_NO_CMS_SW_DIR_MSG} in OSG node"
    produce_message "$SEVERITY_ERROR" "$test_name" "$message" "$message_file"
    echo $message
    exit $ERROR_NO_CMS_SW_DIR
  fi
elif [ -n "$VO_CMS_SW_DIR" ] ; then
  SW_DIR=$VO_CMS_SW_DIR

elif [ -n "$CMS_PATH" ] && [ -d "/cvmfs/cms.cern.ch" ] ; then
  SW_DIR=$CMS_PATH
else
  SW_DIR=/cvmfs/cms.cern.ch
fi

if [ ! -d $SW_DIR ] || [ ! -r $SW_DIR ] ; then
  echo "ERROR: Can not find CMS software dir"
  PSST_ISSUE_TEXT+="ERROR: Can not find CMS software dir, [SwArea=$SW_DIR]\n"
  echo $PSST_ISSUE_TEXT
  exit $ERROR_NO_CMS_SW_DIR
fi

# Software area space
echo -n "Software location: "
if [ "`echo $SW_DIR | cut -d / -f 2`" == "afs" ]; then
  location="AFS"
  echo "$location"
  SPACE=`fs lq $SW_DIR | tail -1 | awk '{print (\$2-\$3)/1024 }'`
elif [ "`echo $SW_DIR | cut -d / -f 2`" == "cvmfs" ]; then
  hasCVMFS=1
  location="CVMFS"
  echo "$location"
else
  location="local"
  echo "$location"
  SPACE=`df -k -P $SW_DIR | tail -1 | awk '{print \$4/1024}'`
fi
message="location=$location"
# Disk space check only for non-CVMFS
if [ $hasCVMFS == 0  ]; then
  echo "FreeSWAreaSpace: $SPACE MB"
  message+=",FreeSWAreaSpace(none-CVMFS)=$SPACE MB"
fi

tmpfile=`mktemp /tmp/tmp.XXXXXXXXXX`
source $SW_DIR/cmsset_default.sh > $tmpfile 2>&1
result=$?
if [ $result != 0 ] ; then
  cat $tmpfile
  rm -f $tmpfile
  echo "ERROR: CMS software initialisation script cmsset_default.sh failed"
  PSST_ISSUE_TEXT+="ERROR: CMS software initialisation script cmsset_default.sh failed,[$message]\n"
  echo $PSST_ISSUE_TEXT
  exit $ERROR_CMSSET_DEFAULT_FAILED
fi
rm -f $tmpfile


if [ $hasCVMFS == 1 ]; then
  echo "Checking CVMFS cache......"
  #approach from /usr/bin/cvmfs_config
  mount_point="/cvmfs/cms.cern.ch"
  cache_use=`df -P $mount_point | tail -n 1 | awk '{print int($3)"/" 1024}' | bc ` 
  cache_avail=`df -P $mount_point | tail -n 1 | awk '{print int($4)"/" 1024}' | bc `
  cache_max=$(($cache_use+$cache_avail))

  echo "INFO: Checking CVMFS cache, [avail=${cache_avail},quota=${cache_max}]"
  PSST_ISSUE_TEXT+="INFO: Checking CVMFS cache, [avail=${cache_avail},quota=${cache_max}].\n"
fi

echo "Default SCRAM_ARCH: $SCRAM_ARCH"

if [ -z "$CMS_PATH" ]; then
  echo "ERROR: CMS_PATH not defined.\n"
  PSST_ISSUE_TEXT+="ERROR: CMS_PATH not defined.\n"
  echo $PSST_ISSUE_TEXT
  exit $ERROR_CMS_PATH_UNDEFINED
fi

if [ ! -d "$CMS_PATH" ]; then
  echo "ERROR: CMS_PATH directory does not exist."
  PSST_ISSUE_TEXT+="ERROR: CMS_PATH directory does not exist.\n"
  echo $PSST_ISSUE_TEXT
  exit $ERROR_CMS_PATH_DIR_MISSING
fi

echo -n "scramv1_version: "
scramv1 version
result=$?
if [ $result != 0 ];
then
  echo "ERROR: scramv1 command not found."
  PSST_ISSUE_TEXT+="ERROR: scramv1 command not found.\n"
  echo $PSST_ISSUE_TEXT
  exit $ERROR_SCRAM_NOT_FOUND
fi

echo "Retrieving list of CMSSW versions installed..."

# get all available cmssw realeases of any architecture
cmssw_installed_version_list=$(scram -a slc* l -a -c CMSSW | awk '{print $2}' |sort -u)

# https://twiki.cern.ch/twiki/bin/view/CMSPublic/PilotStartupSiteTest
cmssw_required_version_list='CMSSW_8_0_25 CMSSW_8_0_26_patch1'
for cmssw_ver in $cmssw_required_version_list
do
  if [ "${cmssw_installed_version_list[@]/$cmssw_ver}" == "${cmssw_installed_version_list[@]}" ]; then
    echo "ERROR: Required CMSSW version not installed."
    PSST_ISSUE_TEXT+="ERROR: Required CMSSW version not installed, [cmssw_ver=$cmssw_ver]\n"
    echo $PSST_ISSUE_TEXT
    exit $ERROR_NO_CMSSW
  else
    #Getting path of cmssw_ver in CVMFS
    cmssw_ver_path=$(scram -a slc* l -a -c ${cmssw_ver} |awk 'END{print $3}')
    # cmssw_ver_path="/cvmfs/cms.cern.ch/slc6_amd64_gcc530/cms/cmssw/CMSSW_8_0_25/src/HLTrigger/Timer/test/chrono/doc"
    echo "Selecting 10 random files $cmssw_ver_path"
    random_files=$(find ${cmssw_ver_path} -maxdepth 3 -type f | shuf -n 10)
    while read -r file; do
      ls_byte_count=$(ls -l "$file" | cut -d " " -f5)
      wc_byte_count=$(wc -c "$file" | cut -d " " -f1)
      if [ "$ls_byte_count" != "$wc_byte_count" ]; then
	echo "ERROR: ls -l  and wc -c  byte_count was not equal."
        PSST_ISSUE_TEXT+="$ERORR_CORRUPTED_CMSSW_FILES_MSG,[file=$file, ls_byte=$ls_byte_count, wc_byte=$wc_byte_count]\n"
	echo $PSST_ISSUE_TEXT
        exit $ERORR_CORRUPTED_CMSSW_FILES
      else
        echo "${file}: ls_byte_count=${ls_byte_count}; wc_byte_count=${wc_byte_count}"
      fi
    done <<< "$random_files"
  fi
done


# -------------------------------------------------------- #
# test to access cvmfs cache, if failed, trigger CVMFS 
# cache error.
# -------------------------------------------------------- #
# add ERROR exit
echo "check if cvmfs cache is not exhausted by opening some big files"
test_dir="/cvmfs/cms.cern.ch/phys_generator/gridpacks"
if [ ! -d "$test_dir" ]; then
  echo "ERROR: Failed to open CVMFS file or directory."
  PSST_ISSUE_TEXT+="ERROR: Failed to open CVMFS file or directory, [test_dir=$test_dir]"
  echo $PSST_ISSUE_TEXT
  exit $ERROR_CVMFS_CACHE_TEST
fi
echo "find some 5 bigest files"
big_files=$(find $test_dir -maxdepth 5 -type f -exec du -Sh {} + | sort -rh | head -n 5)
while read -r file; do
  echo $file
  tail -n 5 $(echo $file | awk '{print $2}') > /dev/null 2>&1
  rc=$?
  if [ "$rc" -ne 0 ]; then
    echo "ERROR: Can not read the test file."
    PSST_ISSUE_TEXT+="ERROR: Can not read the test file, [file=$file].\n"
    echo $PSST_ISSUE_TEXT
    exit $ERROR_CVMFS_READ_TEST_FILE
  fi
done <<< "$big_files"


############################################################
echo "INFO: CMS software is installed correctly."
PSST_ISSUE_TEXT+="INFO: CMS software is installed correctly.\n"
echo $PSST_ISSUE_TEXT
exit 0
