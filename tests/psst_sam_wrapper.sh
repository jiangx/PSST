#!/bin/bash
#
# Description:
#   script to wrapper PSST scripts for SAM
#

# -------------------------------------------------------- #
# initial stuffs for SAM
# -------------------------------------------------------- #
#cp $SAME_SENSOR_HOME/tests/psst_exit_codes.txt .
#cp $SAME_SENSOR_HOME/tests/psst_check_basic.sh .

source $SAME_SENSOR_HOME/tests/psst_exit_codes.txt

psst_scratch="/tmp"
# -------------------------------------------------------- #
# the threshold if to post message
# -------------------------------------------------------- #
INFO_POST_THRESHOLD=1     # post 1% of INFO message
WARNING_POST_THRESHOLD=5  # post 5% of WARNING message
ERROR_POST_THRESHOLD=100  # post 100% of ERROR message

# -------------------------------------------------------- #
# the range of exit_code mapping to severity 
# -------------------------------------------------------- #
RC_OK_MAX=0
RC_INFO_MIN=1
RC_INFO_MAX=20
RC_WARNING_MIN=21
RC_WARNING_MAX=90
RC_ERROR_MIN=91
RC_ERROR_MAX=230

# -------------------------------------------------------- #
# SAME exit_code (from CE-cms-singularity-runner)
# -------------------------------------------------------- #
SAME_OK=10
SAME_INFO=20
SAME_NOTICE=30
SAME_WARNING=40
SAME_ERROR=50
SAME_CRITICAL=60
SAME_MAINTENANCE=100

# -------------------------------------------------------- #
# PSST functions needed in SAM 
# -------------------------------------------------------- #
# exit code mapping to severity 
function prc_to_src(){
    exit_code=$1
    if [[ $exit_code -eq 0 ]]; then
        echo $SAME_OK
    elif [[ $exit_code -ge $RC_NOTICE_MIN ]] && [[ $exit_code -le $RC_NOTICE_MAX ]]; then
        echo $SAME_NOTICE
    elif [[ $exit_code -ge $RC_WARNING_MIN ]] && [[ $exit_code -le $RC_WARNING_MAX ]]; then
        echo $SAME_WARNING
    elif [[ $exit_code -ge $RC_ERROR_MIN ]] && [[ $exit_code -le $RC_ERROR_MAX ]]; then
	echo $SAME_ERROR
    fi
}

function prc_to_ssvr(){
    exit_code=$1
    if [[ $exit_code -eq 0 ]]; then
        echo "OK"
    elif [[ $exit_code -ge $RC_NOTICE_MIN ]] && [[ $exit_code -le $RC_NOTICE_MAX ]]; then
        echo "NOTICE"
    elif [[ $exit_code -ge $RC_WARNING_MIN ]] && [[ $exit_code -le $RC_WARNING_MAX ]]; then
        echo "WARNING"
    elif [[ $exit_code -ge $RC_ERROR_MIN ]] && [[ $exit_code -le $RC_ERROR_MAX ]]; then
	echo "ERROR"
    else
        echo "UNKNOWN"
    fi
}

function fetch_summary(){
    output_log=$1
    echo $output_log |grep "^PSST_ISSUE_TEXT: " | awk -F'PSST_ISSUE_TEXT: ' '{print $2}'
    return
}

function print_without_summary(){
    output_log=$1
    echo -e $output_log |grep -v "^PSST_ISSUE_TEXT: "
    return
}

function generate_psst_exit_code(){
  exit_code_range=10000
  exit_code=$1
  if [ $exit_code -eq 0 ]; then
    echo $exit_code
    return
  else
    psst_exit_code=$(($exit_code_range + $exit_code))
    echo $psst_exit_code
    return
  fi
}

#function fetch_state_by_ecode(){
#    test_file="$1"
#    code_number="$2"
#    declare -A state_names
#    while IFS= read -r var
#    do
#         if [[ $var =~ $regex ]];
#         then
#             key=`echo "$var" | awk -F'=' '{print \$2}'` # exit code
#             value=`echo "$var" | awk -F'=' '{print \$1}'` # error name
#             state_names[$key]="$value"
#         fi
#    done < "$test_file"
#}
function process(){
    rc_code=$1
    tmp_same_rc=`prc_to_src $rc`
    tmp_same_severity=`prc_to_ssvr $rc`
    if [[ $rc_code -ne 0 ]]; then
        psst_exit_code=$(generate_psst_exit_code $rc_code) # display the CMS job exit code in SAM
        rc_list+="#$psst_exit_code"
        state=${state_names[$rc]}
        state_list+="#$state"
        same_severity="$tmp_same_severity"
        same_rc="$tmp_same_rc"
        if [[ "$tmp_same_severity"=="ERROR" ]]; then
            summary+="$same_severity: $state_list; $rc_list"
            echo $summary
            exit $same_rc
        fi
    fi

}

# -------------------------------------------------------- #
# functions for transmit from PSST to SAM
# -------------------------------------------------------- #
# fetch state name and its corresponding exit_code from psst_exit_codes.txt
declare -A state_names
regex="^ERROR_[0-9a-zA-Z_]+=[0-9]+$"
while IFS= read -r var
do
     if [[ $var =~ $regex ]];
     then
         key=`echo "$var" | awk -F'=' '{print \$2}'` # exit code
         value=`echo "$var" | awk -F'=' '{print \$1}'` # error name
         state_names[$key]="$value"
     fi
done < "psst_exit_codes.txt"

#psst_tmp_log=$psst_scratch"/psst_tmp_$$.log"
declare -a psst_tmp_log
rc_list=""
state_list=""
summary="summary: "
same_severity="OK"
same_rc=$SAME_OK

################### check_basic ############################
date_utc=`date --utc +"%Y-%m-%d %H:%M:%S"`
test_name="check_basic"
echo "Executing $test_name at $date_utc ------------------"
psst_tmp_log=$(. ./psst_check_basic.sh) 
rc=$?
echo "${psst_tmp_log}" | grep -v "^PSST_ISSUE_TEXT: "
#echo "${psst_tmp_log}"
process $rc
echo

# ############### end area ############################### #
summary+="$same_severity: $rc_list"
echo $summary
exit $same_rc

