#!/bin/bash
#
# Description:
#   Script to check quata and free space for scratch
#

############################################################
PSST_ISSUE_TEXT="PSST_ISSUE_TEXT: "
exit_code=0

# set default number of cpu 
cpus="$1"
if [ "x$cpus" = "x" ]; then
	cpus=1
fi

work_dir="$2"
echo "cpus: $cpus; work_dir: $work_dir"

# -------------------------------------------------------- #
# define the space threshold
# -------------------------------------------------------- #
#20 GB of scratch sapce per code, defined in VO card
required_space=$((20000 * cpus))
#if site has lower than 5 GB of scratch space, validation will fail
critical_space=$((5000 * cpus))
echo "Required scratch space: ${required_space} MB"
#Required 10MB in /TMP
required_space_tmp=10

# -------------------------------------------------------- #
# check if work directory exists
# -------------------------------------------------------- #
if [ ! -n "$work_dir" ] || [ ! -d "$work_dir" ]; then
    echo "ERROR: scratch dir was not found."
    PSST_ISSUE_TEXT+="ERROR: scratch dir was not found, [work_dir=$work_dir]. \n"
    echo $PSST_ISSUE_TEXT
    exit $ERROR_SCRATCH_DIR_NOT_FOUND
fi

# -------------------------------------------------------- #
# check if there are fatal space used
# -------------------------------------------------------- #
free_space=$(df -Ph -B1MB "$work_dir" | awk '{if (NR==2) print $4}')
echo "Scratch free space in ${work_dir}: ${free_space} MB"

disk_size=$(df -Ph -B1MB "$work_dir" | awk '{if (NR==2) print $2}')
echo "Scratch used space in ${work_dir}: ${disk_size} MB"

free_space_tmp=$(df -P -B1MB /tmp | awk '{if (NR==2) print $4}')
echo "Free space in /tmp ${free_space_tmp} MB"

if [ "$free_space_tmp" -lt "$required_space_tmp" ]; then
    echo "WARNING: less than 10MB free in /tmp."
    PSST_ISSUE_TEXT+="WARNING: less than 10MB free in /tmp, [tmp_dir=/tmp,free_space_tmp=${free_space_tmp}].\n"
fi

if [ "$free_space" -lt "$required_space" ]; then
    if [ "$free_space" -lt "$critical_space" ]; then
	echo "ERROR: less than 5 GB/core of free space in scratch dir."
	PSST_ISSUE_TEXT+="ERROR: less than 5 GB/core of free space in scratch dir, [work_dir=${work_dir},free_space_scr=${free_space},used_space=${disk_size}].\n"
	echo $PSST_ISSUE_TEXT
    	exit $ERROR_LOW_SCRATCH_SPACE
    else
	echo "WARNING: less than 20 GB of free space per core in scratch dir."
	PSST_ISSUE_TEXT+="WARNING: less than 20 GB of free space per core in scratch dir, [work_dir=${work_dir},free_space_scr=${free_space},used_space=${disk_size}]"
    fi
fi

# -------------------------------------------------------- #
# echo INFO if there's no ERROR or CRITICAL 
# -------------------------------------------------------- #
echo "INFO: Scratch space test is OK, [work_dir=${work_dir},free_space_scr=${free_space},used_space=${disk_size}]."
PSST_ISSUE_TEXT+="INFO: Scratch space test is OK, [work_dir=${work_dir},free_space_scr=${free_space},used_space=${disk_size}].\n"
echo $PSST_ISSUE_TEXT
exit 0
