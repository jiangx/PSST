#!/bin/bash
#
# Description:
#   Script to check if proxy is valid, specific dirs exist
#

############################################################
PSST_ISSUE_TEXT="PSST_ISSUE_TEXT: "
exit_code=0

# -------------------------------------------------------- #
# Software area definition and existence
# -------------------------------------------------------- #
# checking X509 certificate
if [ -e "$X509_CERT_DIR" ]; then
    cert_dir=$X509_CERT_DIR
elif [ -e "$HOME/.globus/certificates/" ]; then
    cert_dir=$HOME/.globus/certificates/
elif [ -e "/etc/grid-security/certificates/" ]; then
    cert_dir=/etc/grid-security/certificates/
else
    echo "ERROR: could not find X509 certificate directory."
    PSST_ISSUE_TEXT+="ERROR: could not find X509 certificate directory, [cert_dir=${cert_dir}].\n"
    echo $PSST_ISSUE_TEXT
    exit $ERROR_NO_CERT_DIR
fi
echo "CertDir: $cert_dir"

# checking X509 user proxy
if [ -a "$X509_USER_PROXY" ]; then
    proxy=$X509_USER_PROXY
elif [ -a "/tmp/x509up_u`id -u`" ]; then
    proxy="/tmp/x509up_u`id -u`"
else
    echo "ERROR: could not find X509 proxy certificate."
    PSST_ISSUE_TEXT+="ERROR: could not find X509 proxy certificate, [proxy=${proxy}]"
    echo $PSST_ISSUE_TEXT
    exit $ERROR_NO_X509_PROXY
fi
echo "Proxy: $proxy"

# checking voms-proxy-info
type -t voms-proxy-info
result=$?
if [ $result -eq 0 ] ; then
    isvoms=1
    echo -n "VOMS server: "
    voms-proxy-info --uri
    echo -n "UserDN: "
    voms-proxy-info -identity
    time_left=$(voms-proxy-info -timeleft)
    echo "Timeleft: $time_left s"
    fqan=$(voms-proxy-info -fqan)
    echo "FQAN:"
    echo "$fqan"
else
    isvoms=0
    echo "WARNING: voms-proxy-info not found."
    PSST_ISSUE_TEXT+="WARNING: voms-proxy-info not found.\n"
fi
if [ $isvoms -eq 1 -a $time_left -lt 21600 ] ; then
    echo "WARNING: proxy shorther than 6 hours."
    PSST_ISSUE_TEXT+="WARNING: proxy shorther than 6 hours, [isvoms=$isvoms,timeleft=$time_left].\n"
fi

############################################################
echo "INFO: Proxy test is OK."
PSST_ISSUE_TEXT+="INFO: Proxy test is OK.\n"
exit 0
