#!/bin/bash
#
# Pilot Startup Site Test wrapper script
#


glidein_config="$1"
time_limit=300
#email_addr="xiaowei.jiang@cern.ch"

RUNNING_IN_PSST="TRUE"
export RUNNING_IN_PSST


PSST_TAR_DIR=$(grep -m1 -i '^GLIDECLIENT_CMS_PSST' $glidein_config | awk '{print $2}')
export PSST_TAR_DIR

timeout $time_limit ${PSST_TAR_DIR}/psst.sh "$glidein_config" "$PSST_TAR_DIR"
exit_code=$?

if [ "$exit_code" -ge 124 -a "$exit_code" -le 137 ];then
  echo "PSST reached time limit - ${time_limit} s"
  site_name=$(grep -m1 -i '^GLIDEIN_CMSSite' $glidein_config | awk '{print $2}')
  hostname | /usr/bin/Mail -s "PSST reached time limit - ${time_limit} s at ${site_name}" $email_addr
  exit 0
else
  exit $exit_code
fi
