#!/bin/bash
#
# Project:
#   PSST
# Description:
#   Pilot Startup Site Test main script
#

echo "###### Executing Pilot Startup Site Test Version 1.1.5 #######"
# check the needed commands(rpms,commands), installed particular rpms.

############################################################
# source the variables and functions needed 
############################################################
# path of glidein_config file
glidein_config="$1" 

# Find directory of sub-scripts 
my_tar_dir="$2"

# source libs and configs
. $my_tar_dir/local_libs.sh # libs.sh for prod, local_libs.sh for testing
. $my_tar_dir/exit_codes.txt # exit status attributes
. $my_tar_dir/psst_config

# -------------------------------------------------------- #
# define the necessary static variables
# -------------------------------------------------------- #
start_timestamp=$(date -u +%s%3N) # UTC time
email_addr=$admin_email # operator's email

log_file=$scratch"/psst_$$.log" # temporary log file
message_file=$scratch"/psst_message_$$.txt" # temporary message
psst_tmp_file=$scratch"/psst_tmp_$$.txt"

# -------------------------------------------------------- #
# PSST attributes, which should be set into glidein config
# and job classad
# -------------------------------------------------------- #
#PSST_CMS_SITENAME=""
#PSST_HOSTNAME=""
PSST_ISSUE_CODES=""
PSST_ISSUE_TEXT=""
PSST_ISSUE_SEVERITY=""

############################################################

# -------------------------------------------------------- #
# if script was interupted, remove the temporary log file
# -------------------------------------------------------- #
trap "rm $log_file; rm $message_file; exit" SIGHUP SIGINT SIGTERM

# -------------------------------------------------------- #
# Redirect stdout and stderr ( > ) into a named pipe 
# ( >() ) running "tee"
# -------------------------------------------------------- #
exec &>  >(tee -ia $log_file)

# -------------------------------------------------------- #
# print out basic information
# -------------------------------------------------------- #
echo "More information - https://twiki.cern.ch/twiki/bin/view/CMSPublic/PilotStartupSiteTest"

############################################################
# check the expected attributes from glidein_config 
############################################################

echo "Useful attributes related to GLIDEIN......"

# grep add_config_line_source from glidein_config and source 
# the related functions
add_config_line_source=`awk '/^ADD_CONFIG_LINE_SOURCE /{print $2}' $glidein_config`
alert_error $? "Can't find add_config_line_source" $log_file
echo "  add_config_line_source: " $add_config_line_source
source $add_config_line_source

# grep the condor_vars_file, which psst attrs would be added into
condor_vars_file=`grep -i "^CONDOR_VARS_FILE " $glidein_config | awk '{print $2}'`
alert_error $? "Can't find condor_vars_file" $log_file
echo "  condor_vars_file: " $condor_vars_file

# # Find directory of sub-scripts 
# my_tar_dir=$(grep -m1 -i '^GLIDECLIENT_CMS_PSST ' $glidein_config | awk 'END { if (NR==0 || $2=="")  exit 1; else print $2;}')
# alert_error $? "Can't find directory of sub-scripts" $log_file
# echo "  directory of PSST sub-scripts: "$my_tar_dir

# Grep site_name from glidein_config
site_name=$(grep -m1 -i '^GLIDEIN_CMSSite' $glidein_config| awk 'END { if (NR==0 || $2=="")  exit 1; else print $2;}')
alert_error $? "Can't find site_name" $log_file
echo "  CMS site name: " $site_name

# Grep workdir from glidein_config
work_dir=$(grep -m1 -i '^GLIDEIN_WORK_DIR' $glidein_config|awk 'END { if (NR==0 || $2=="")  exit 1; else print $2;}')
alert_error $? "Can't find work directory" $log_file
echo "  work dir: " $work_dir

# Grep CE from glidein_config
target_ce=$(grep -m1 -i '^GLIDEIN_Gatekeeper' $glidein_config| awk 'END { if (NR==0 || $2=="")  exit 1; else print $2;}' | cut -f1 -d":")
alert_error $? "Can't find CE name" $log_file
echo "  CE name: " $target_ce

# # Grep CE flavour
# grid_type=$(grep -m1 -i '^GLIDEIN_GridType' $glidein_config|awk 'END { if (NR==0 || $2=="")  exit 1; else print $2;}')
# alert_error $? "Can't find CE type" $log_file
# echo "  GridType: " $grid_type
# 
# # CE flavour as GridType
# if [ "$grid_type" = "gt2" ]; then
#   ce_flavour="GLOBUS"
# elif [ "$grid_type" = "nordugrid" ]; then
#   ce_flavour="ARC-CE"
# elif [ "$grid_type" = "cream" ]; then
#   ce_flavour="CREAM-CE"
# elif [ "$grid_type" = "condor" ]; then
#   ce_flavour="HTCONDOR-CE"
# else
#   ce_flavour=$grid_type
# fi
# echo "  CE flavour: " $ce_flavour

# Grep number of glidein CPUs
cpus=$(grep -m1 -i '^GLIDEIN_CPUS' $glidein_config| awk 'END { if (NR==0 || $2=="")  exit 1; else print $2;}')
#2017-03-20 many factory entries are missing GLIDEIN_CPUS field in glidein
#config, by default it is set to 1
if [ "X$cpus" = X ]; then
  echo "No GLIDEIN_CPUS field in glidein_config, it's defautly set to 1"
  cpus=1
fi
alert_error $? "Can't find number of CPUs" $log_file
echo "  Number of CPUs:" $cpus


############################################################
# add new PSST attributes to glidein config and condor startd 
# classad
############################################################
function joint_issue_codes(){
    if [ $1 -ne 0 ];
    then
	psst_exit_code=$(generate_psst_exit_code $exit_code)
	PSST_ISSUE_CODES+="${psst_exit_code};"
    fi 
}

function post_message(){
    # ------------------- PSST_TIMESTAMP --------------------- #
    echo "adding PSST_TIMESTAMP to startd classad"
    PSST_TIMESTAMP=$start_timestamp
    echo $PSST_TIMESTAMP
    add_config_line "PSST_TIMESTAMP" "$PSST_TIMESTAMP"
    add_condor_vars_line "PSST_TIMESTAMP" "I" "0" "+" "N" "Y" "-"
    
    # ------------------ PSST_ISSUE_CODES -------------------- #
    echo "adding PSST_ISSUE_CODES to startd classad"
    echo $PSST_ISSUE_CODES
    add_config_line "PSST_ISSUE_CODES" "$PSST_ISSUE_CODES"
    add_condor_vars_line "PSST_ISSUE_CODES" "S" "0" "+" "N" "Y" "-"
    
    # ------------------ PSST_ISSUE_TEXT --------------------- #
    echo "reading message from message file and adding PSST_ISSUE_TEXT to startd classad"
    PSST_ISSUE_TEXT=$(echo $(<$message_file))
    echo $PSST_ISSUE_TEXT
    add_config_line "PSST_ISSUE_TEXT" "${PSST_ISSUE_TEXT}"
    add_condor_vars_line "PSST_ISSUE_TEXT" "S" "None" "+" "N" "Y" "-"
    
    # ----------------- PSST_ISSUE_SEVERITY ------------------ #
    echo "adding PSST_ISSUE_SEVERITY to startd classad"
    PSST_ISSUE_SEVERITY=$TEMP_SEVERITY
    echo $PSST_ISSUE_SEVERITY
    add_config_line "PSST_ISSUE_SEVERITY" "$PSST_ISSUE_SEVERITY"
    add_condor_vars_line "PSST_ISSUE_SEVERITY" "S" "INFO" "+" "N" "Y" "-"
    
    # ---------------------- End Area ------------------------ #
    cat $glidein_config
    [[ -x $log_file ]] && rm $log_file
    [[ -x $message_file ]] && rm $message_file
    # exit $exit_code
    exit 0 # for test, exit_code was always set to 0
}

function if_post_message(){
    #TEMP_SEVERITY=$(awk -F': ' 'END{print $1}' $message_file)
    tmp_severity=$1
    if [ "$tmp_severity" == "CRITICAL" ] || [ "$tmp_severity" == "ERROR" ]; then
	post_message
    fi
}

function message_process(){
    log_details=$1
    exit_code=$2
    echo "${psst_tmp_log}" | grep -v "^PSST_ISSUE_TEXT: "
    joint_issue_codes $exit_code
    severity=$(exitcode_to_severity $exit_code)
    sub_message=$(echo "${psst_tmp_log}" |grep "^PSST_ISSUE_TEXT: "|awk -F"PSST_ISSUE_TEXT: " '{print $2}' )
    echo -e $sub_message | while read -r line; 
    do
        sub_line_svr=$(echo $line | awk -F": " '{print $1}')
        add_message $sub_line_svr $test_name "${line}" $message_file
    done 
    if_post_message
    
}
############################################################
# call the tests in order
############################################################

echo "############## start of basic test #######################"
test_name="basic"
#${my_tar_dir}/tests/psst_check_basic.sh $my_tar_dir $test_name > $psst_tmp_file
psst_tmp_log=$(. ${my_tar_dir}/tests/psst_check_basic.sh)
exit_code=$?
echo "${psst_tmp_log}" | grep -v "^PSST_ISSUE_TEXT: "
joint_issue_codes $exit_code
severity=$(exitcode_to_severity $exit_code)
echo "severity: $severity"
sub_message=$(echo "${psst_tmp_log}" |grep "^PSST_ISSUE_TEXT: "|awk -F"PSST_ISSUE_TEXT: " '{print $2}' )
echo -e $sub_message | while read -r line; 
do
    sub_line_svr=$(echo $line | awk -F": " '{print $1}')
    add_message $sub_line_svr $test_name "${line}" $message_file
done 
if_post_message
echo "######## end of basic test with exit code $exit_code #####"
echo

echo "############## start of connection test ##################"
psst_tmp_log=$(. ${my_tar_dir}/tests/psst_check_connection.sh)
exit_code=$?
message_process "${psst_tmp_log}" $exit_code
echo "##### end of connection test with exit code $exit_code ###"
echo

echo "############## start of cpu load test ####################"
psst_tmp_log=$(. ${my_tar_dir}/tests/psst_check_cpuload.sh $cpus)
exit_code=$?
message_process "${psst_tmp_log}" $exit_code
echo "###### end of cpu load test with exit code $exit_code ####"
echo

echo "############## start of software test ####################"
psst_tmp_log=$(. ${my_tar_dir}/tests/psst_check_software_area.sh $cpus)
exit_code=$?
message_process "${psst_tmp_log}" $exit_code
echo "###### end of software test with exit code $exit_code ####"
echo

echo "############## start of scratch space test ###############"
psst_tmp_log=$(. ${my_tar_dir}/tests/psst_check_scratch_space.sh $cpus "$work_dir")
exit_code=$?
message_process "${psst_tmp_log}" $exit_code
echo "### end of scratch space test with exit code $exit_code ##"
echo

echo "################### start of proxy test ##################"
psst_tmp_log=$(. ${my_tar_dir}/tests/psst_check_proxy.sh) 
exit_code=$?
message_process "${psst_tmp_log}" $exit_code
echo "###### end of proxy test with exit code $exit_code #######"
echo

echo "################# start of siteconf test #################"
psst_tmp_log=$(. ${my_tar_dir}/tests/psst_check_siteconf.sh)
exit_code=$?
message_process
echo "### end of siteconf test with exit code $exit_code #######"
echo

# echo "################# start of squid test ####################"
# ${my_tar_dir}/tests/test_squid.sh $glidein_config $my_tar_dir
# exit_code=$?
# psst_exit_code=$(generate_psst_exit_code $exit_code)
# PSST_ISSUE_CODES+="$psst_exit_code"";"
# echo "###### end of squid test with exit code $exit_code #######"
# is_post_info
# echo
# 
# echo "############### start of singularity test ################"
# ${my_tar_dir}/tests/check_singularity.sh 
# exit_code=$?
# psst_exit_code=$(generate_psst_exit_code $exit_code)
# PSST_ISSUE_CODES+="$psst_exit_code"";"
# echo "### end of singularity test with exit code $exit_code ####"
# is_post_info
# echo

# ---------------------- End Area ------------------------ #
exit_code=0
post_info
exit $exit_code

