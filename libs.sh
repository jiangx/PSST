#!/bin/bash
#
# Project:
#   PSST
# Discription:
#   libraries and functions required in PSST
#

# -------------------------------------------------------- # 
# calculate the PSST exit code, psst uses range(10000-19999)
# of job exit codes
# -------------------------------------------------------- # 
function generate_psst_exit_code(){
  exit_code_range=10000
  exit_code=$1
  if [ $exit_code -eq 0 ]; then
    echo $exit_code
    return
  else
    psst_exit_code=$(($exit_code_range + $exit_code))
    echo $psst_exit_code
    return
  fi
}

# -------------------------------------------------------- # 
# In case of unexpected script failure, email alert will be
# sent and PSST exit with 0
# -------------------------------------------------------- # 
function alert_error(){
  exit_code=$1
  log_file=$2

  if [ $exit_code -ne 0 ]; then
    hostname=$(hostname)
    MSG="PSST ERROR: $exit_code; hostname: $hostname"
    echo "$MSG"
    cat $log_file | /usr/bin/Mail -s "${MSG}" ${email_addr}
    rm $log_file
    exit 0
  fi
}

# -------------------------------------------------------- # 
# functions for message processing
# -------------------------------------------------------- # 

function fetch_summary(){
    output_file=$1
    grep "^PSST_ISSUE_TEXT: " $output_file | awk -F'PSST_ISSUE_TEXT: ' '{print $2}'
    return
}

function print_without_summary(){
    #output_file=$1
    log=$1
    echo $log | grep -v "^PSST_ISSUE_TEXT: "
    return
}

# -------------------------------------------------------- # 
# produce the psst message as the pre-defined format
# -------------------------------------------------------- # 
function produce_message(){
  severity=$1
  test_name=$2
  message="$3"
  message_file=$4

  echo "$severity: [$test_name]: $message\n" >> $message_file
  return
}

# -------------------------------------------------------- # 
# generate a random number between 0 and 99(include 0 and 99)
# -------------------------------------------------------- # 
function gen_rand_number(){
  magnification=100
  random_number=$((awk -v magn=$magnification 'BEGIN{srand(); print int(rand()*magn)}'))
  echo $random_number
  return
}

# -------------------------------------------------------- # 
# produce message in proportion
# -------------------------------------------------------- # 
function add_message(){
    severity=$1
    test_name=$2
    message="$3"
    message_file=$4

    random_number=$((gen_rand_number))
    if [[ $severity == "INFO" ]] && [[ $random_number -lt $INFO_POST_THRESHOLD ]]; then
	produce_message "$severity" "$test_name" "$message" "$message_file"
    elif [[ $severity == "WARNING" ]] && [[ $random_number -lt $WARNING_POST_THRESHOLD ]]; then
	produce_message "$severity" "$test_name" "$message" "$message_file"
    elif [[ $severity == "ERROR" ]] && [[ $random_number -lt $ERROR_POST_THRESHOLD ]]; then
	produce_message "$severity" "$test_name" "$message" "$message_file"
    fi	
}

# -------------------------------------------------------- # 
# exit code mapping to severity 
# -------------------------------------------------------- # 
function exitcode_to_severity(){
    exit_code=$1
    if [[ $exit_code -eq 0 ]]; then
	echo $SEVERITY_INFO
    elif [[ $exit_code -ge $RC_NOTICE_MIN ]] && [[ $exit_code -le $RC_NOTICE_MAX ]]; then
	echo $SEVERITY_NOTICE
    elif [[ $exit_code -ge $RC_WARNING_MIN ]] && [[ $exit_code -le $RC_WARNING_MAX ]]; then
	echo $SEVERITY_WARNING
    elif [[ $exit_code -ge $RC_ERROR_MIN ]] && [[ $exit_code -le $RC_ERROR_MAX ]]; then
	echo $SEVERITY_ERROR
    fi
}
